$(document).ready(function(){
	$('.popup-switcher').click(function(){
		var url = $(this).attr('href');
		$(this).addClass('active');
		$(url).fadeIn();
		$('.modal-overlay').fadeIn();
		return false;
	});
	$('.modal-overlay, .popup-hide, .active').on('click',function(){
		$('.popup-switcher').each(function(){
			var url = $(this).removeClass('active').attr('href');
			$(url).fadeOut();
			$('.modal-overlay').fadeOut();
		});
	});

	$('.checkbox-box').niceCheckbox();
	$('.radio-box').niceCheckbox();
});

(function($){
    jQuery.fn.niceCheckbox = function(options){
    var make = function(){
        var radio = $(this).find('input:radio');
        radio.hide().after('<span class="gicon gicon-radio"></span>');
        radio.each(function(){
            if($(this).is(":checked")){
                $(this).next('.gicon-radio').addClass('on');
            }
        });
        radio.change(function(){
        	var name = $(this).attr('name');
        	$('input[name='+name+']').attr('checked', false).next(".gicon-radio").removeClass("on");
        	$(this).attr('checked',true).next(".gicon-radio").addClass("on");
            // if($(this).attr('checked')){
            //     $(this).attr('checked', false).next(".gicon-radio").removeClass("on");
            // }else{
            //     $(this).attr('checked',true).next(".gicon-radio").addClass("on"); 
            // }         
        });
        radio.next('.gicon-radio').click(function(){
            $(this).prev().trigger('click');
        }); 

        var checkbox = $(this).find('input:checkbox');
        checkbox.hide().after('<span class="icon gicon-checkbox"></span>');
        checkbox.each(function(){
            if($(this).is(":checked")){
                $(this).next('.gicon-checkbox').addClass('on');
            }
        });
        checkbox.change(function(){
            if($(this).attr('checked')){
                $(this).attr('checked', false).next(".gicon-checkbox").removeClass("on");
            }else{
                $(this).attr('checked',true).next(".gicon-checkbox").addClass("on"); 
            }          
        });
        checkbox.next('.gicon-checkbox').click(function(){
            $(this).prev().trigger('click');
            $(this).parent('label').trigger('click');
        });        
    };
    return this.each(make);
};
})(jQuery);